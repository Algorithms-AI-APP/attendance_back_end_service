# -*- coding: utf-8 -*-

import os
import base64

import numpy as np

import face_recognition

# 从人脸库中计算已知人脸特征值
def get_known_faces_encoding(path):
    known_names=[] # 存储已知人名
    known_encodings=[]  # 存储知道的特征值
    for image_name in os.listdir(path):
        load_image = face_recognition.load_image_file(path + image_name)
        image_face_encoding = face_recognition.face_encodings(load_image)[0] # 获得128维特征值
        known_names.append(image_name.split(".")[0])
        known_encodings.append(image_face_encoding)
    return [known_encodings, known_names]  # type: np.array
    # print(known_names)
    # print(known_encodings)

# 判断人脸是否在人脸库中
def judge_face_is_in_data_set(file_stream, data_set_path):

    img = face_recognition.load_image_file(file_stream)

    unknown_face_encodings = face_recognition.face_encodings(img)

    face_found = False
    # face_name = ''
    sign_in_name= ''  # 签到成功人员的姓名

    status = 'failed'  # 上传的人脸默认不在库中
    if len(unknown_face_encodings) > 0:
        face_found = True

        [known_encodings, known_names] = get_known_faces_encoding(data_set_path)
        # print(known_encodings)
        # print(unknown_face_encodings)
        # TypeError: unsupported operand type(s) for -: 'list' and 'list'
        # issue: https://github.com/ageitgey/face_recognition/issues/148
        # matches = face_recognition.compare_faces(known_encodings, unknown_face_encodings)
        matches = face_recognition.compare_faces(known_encodings, np.array(unknown_face_encodings))
        # print(matches)
        # print(known_names)
        if True in matches:
            status = 'success'
            sign_in_name = known_names[matches.index(True)]
            # print(known_names[matches.index(True)])
    result = {
        'face_found_in_image': face_found,
        'sign_in_state': status  # 签到状态
    }
    # print(result)
    return [result, sign_in_name]

# 将 base64 编码还原为图片
def base64_to_img(file_base64_str, filename, path):
    imgdata = base64.b64decode(bytes(file_base64_str, encoding='utf-8'))
    file = open(path + filename, 'wb')
    # print(path + filename)  # 文件存放的路径
    file.write(imgdata)  # str.encode(‘utf-8’)
    file.close()

def img_to_base64(filename):
    with open(filename, "rb") as f:
        base64_data = base64.b64encode(f.read())
        return base64_data
        file = open('result.txt','wt')
        file.write(base64_data.decode('utf-8'))  # bytes.decode('utf-8')
        file.close()
