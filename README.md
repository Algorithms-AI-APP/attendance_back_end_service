## 食用指南

> 开发环境：MySQL 8.0、Python 3.7.3

```bash
# create virtual environment
virtualenv env
# activate virtual environment
source env/Scripts/activate
# install dependencies
pip install -r requirements.txt
# start flask service
flask run  # served at http://localhost:5000/
```

数据库配置文件 `mysql.cfg`

```text
[mysql]

host = localhost

port = 3306

user = root

database = mystudent

password = 123456

charset = UTF8MB4
```

**注意**：服务写了几个自定义 `flask` 命令，可通过这几个命令创建/删除数据库表 `checking_in`

```bash
flask
Commands:
  create-api    create api info table
  create-table  create table checking_in
  delete-table  delete table checking_in
```

## 附注

创建数据库

```sql
create database mystudent;
use mystudent;
```

创建考勤记录表

```sql
CREATE TABLE checking_in (
    id INT PRIMARY KEY AUTO_INCREMENT,
    num INT NOT NULL,
    name VARCHAR(10) NOT NULL,
    sex ENUM('male','female') NOT NULL DEFAULT 'male',
    date DATETIME ,
    status ENUM('0','1','2','3') NOT NULL DEFAULT '3'
);
```