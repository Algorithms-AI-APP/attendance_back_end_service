# -*- coding: utf-8 -*-

import os
import sys
import click
import logging

import pymysql
# import pandas as pd
from time import strftime, gmtime
# from pymongo import MongoClient
# from sqlalchemy import create_engine

from flask import Flask
from werkzeug.utils import secure_filename
from flask_cors import CORS
from flask import jsonify, make_response, abort, request, url_for

from utils import base64_to_img, judge_face_is_in_data_set


##########################################################################
# 日志体系

logger = logging.getLogger(__name__)
logger.setLevel(level = logging.INFO)
handler = logging.FileHandler("./log.txt", encoding='utf-8')
handler.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)

# 终端打印注册
console = logging.StreamHandler()
console.setLevel(logging.INFO)

logger.addHandler(console)

##########################################################################
# 服务体系

# 允许上传文件类型
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])

# 校验文件类型
def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


app = Flask(__name__)

CORS(app)

# TODO: 使用环境变量，生产线上 Linux 的路径可能不太合适
app.config['UPLOAD_FOLDER'] = os.getcwd() + '\\waiting_judge_face\\'   # 待验证人脸暂存目录
app.config['MAX_CONTENT_LENGTH'] = 4096 * 1024 * 1024
app.config['FACES_DATA_SET'] = os.getcwd() + '\\faces_data_set\\'      # 人脸仓库

# 生产线上的配置
# app.config['UPLOAD_FOLDER'] = './waiting_judge_face/'   # 待验证人脸暂存目录
# app.config['FACES_DATA_SET'] = './faces_data_set/'      # 人脸仓库

# CROSS-ORIGIN-RESOURCE-SHARING：跨域资源共享
cors = CORS(app, resources={r"/api/*": {"origin": "*"}})

##########################################################################

# 获取 apis 信息： GET /api/v1/info
@app.route("/api/v1/info")
def apis():

    connection = pymysql.connect(read_default_group='mysql', read_default_file = './mysql.cfg')

    api_list = []

    try:
        with connection.cursor() as cursor:
            # Read a single record
            sql = "SELECT * FROM apirelease"
            cursor.execute(sql)
            results = cursor.fetchall()

            for row in results:
                api = {}
                api['buildtime'] = row[0]
                api['version'] = row[1]
                api['methods'] = row[2]
                api['links'] = row[3]
                api_list.append(api)
    except Exception as e:
        connection.rollback()
        logging.exception(e)  # 打印错误信息堆栈
    finally:
        connection.close()
    # print(api_list)

    return jsonify({'api_version':api_list}), 200

##########################################################################

# 获取所有考勤记录：GET /api/v1/attendance-records
@app.route('/api/v1/attendance-records', methods= ['GET'])
def get_attendance_records():
    return attendance_records_list()

def attendance_records_list():
    connection = pymysql.connect(read_default_group='mysql', read_default_file = './mysql.cfg')

    # 考勤记录列表
    attendance_records_list = []

    try:
        with connection.cursor() as cursor:
            logger.info("管理员正在获取所有考勤记录...")

            # Read a single record
            sql = "SELECT * FROM checking_in"
            cursor.execute(sql)
            results = cursor.fetchall()

            # 测试日志服务
            # raise RuntimeError('testError')

            for row in results:
                attendance_record = {}
                # attendance_record['id'] = row[0]
                attendance_record['key'] = row[0]
                attendance_record['student_number'] = row[1]
                attendance_record['name'] = row[2]
                attendance_record['sex'] = row[3]
                attendance_record['date'] = row[4].strftime('%Y-%m-%d %H:%M:%S')
                attendance_record['status'] = row[5]
                attendance_records_list.append(attendance_record)

            logger.info("获取所有考勤记录成功")
            logger.info(results)
    except Exception as e:
        logger.error("获取考勤记录失败！")
        logger.error(e)
    finally:
        connection.close()

    return jsonify({"records": attendance_records_list})

##########################################################################

# 添加一条新的考勤记录：POST /api/v1/attendance-records
@app.route ('/api/v1/attendance-records', methods=['POST'])
def create_attendance_record():

    if not request.json or not 'student_number' in request.json or not \
        'name' in request.json:
            logger.warning("新增考勤记录：POST 的字段不完整、有缺失")
            logger.warning(request.json)
            abort(400)

    # TODO：没协商好
    new_record = {
        'num': request.json['student_number'],
        'name': request.json['name'],
        'sex': 'male',
        'date': request.json['date'],
        'status': 3
    }

    return jsonify({'status': add_attendance_record(new_record)}), 210

def add_attendance_record(new_record):

    connection = pymysql.connect(read_default_group='mysql', read_default_file = './mysql.cfg')

    already_exists = False  # 用于标识新增的记录是否已存在

    try:
        with connection.cursor() as cursor:
            # Read a single record
            sql = "SELECT * FROM checking_in"
            cursor.execute(sql)
            results = cursor.fetchall()

            for row in results:
                if row[1] == new_record['num'] or row[2] == new_record['name']:
                    already_exists = True
            if already_exists:  # 要新增的考勤记录已存在
                abort(400)
            else:
                logger.info("正在新增一条考勤记录...")
                cursor.execute("insert into checking_in(num, name, sex, date, status) values(%s,%s,%s,%s,%s)",\
                    (new_record['num'], new_record['name'], new_record['sex'], new_record['date'], new_record['status']))
                logger.info("增加考勤记录成功")
                connection.commit()
                return "Success"
    except Exception as e:
            # logging.exception(e)  # 打印错误信息堆栈
            logger.error("添加考勤记录失败")
            logger.error(e)
            connection.rollback()
    finally:
        connection.close()
    return "Failed"

##########################################################################

# 删除指定 id（key） 的单条记录：DELETE /api/v1/attendance-records
@app.route('/api/v1/attendance-records', methods=['DELETE'])
def delete_attendance_record():

    if not request.json or not 'key' in request.json:
        logger.warning("删除考勤记录：DELETE 的字段不完整、有缺失")
        logger.warning(request.json)
        abort(400)

    waiting_delete_record_id = request.json['key']
    print(waiting_delete_record_id)
    return jsonify({'status': del_record(waiting_delete_record_id)}), 200

# 删除指定 id 的记录
def delete_record(id_):

    connection = pymysql.connect(read_default_group='mysql', read_default_file = './mysql.cfg')

    try:
        with connection.cursor() as cursor:
            logger.info("删除考勤记录中...")
            cursor.execute("delete from checking_in where id=%s", id_)
            connection.commit()
            logger.info("删除成功")
    except Exception as e:
        connection.rollback()
        logger.error("删除考勤记录失败")
        logger.error(e)  # 打印错误信息堆栈
        return "Failed"
    finally:
        connection.close()

# 检查要删除记录的
def del_record(del_id):
    if is_exist_key(del_id):
        delete_record(del_id)
        return "Success"
    else:
        logger.error("删除考勤记录失败")
        return "Failed"

##########################################################################

# 检查指定 key（id）的记录是否已经存在
def is_exist_key(key_):

    connection = pymysql.connect(read_default_group='mysql', read_default_file = './mysql.cfg')

    already_exist = False
    try:
        with connection.cursor() as cursor:
            sql = "SELECT * FROM checking_in"
            cursor.execute(sql)
            results = cursor.fetchall()

            for row in results:
                if row[0] == key_:
                    already_exist = True
                    break

            if already_exist:
                return True
            else:
                return False
    except Exception as e:
        logger.error(e)  # 打印错误信息堆栈
    finally:
        connection.close()

##########################################################################

# 修改指定 key（id）的记录
@app.route('/api/v1/attendance-records', methods = ['PUT'])
def update_attendance_record():
    # print(request.json)
    if not request.json or not 'key' in request.json or not 'student_number' in request.json or not \
        'name' in request.json or not 'date' in request.json:
        logger.warning("更新考勤记录：PUT 的字段不完整、有缺失")
        logger.warning(request.json)
        abort(400)

    update_record = {}
    record_keys = request.json.keys()

    for x in record_keys:
        if x == 'key':
            update_record['id'] = request.json['key']
        else:
            update_record[x] = request.json[x]
    return jsonify({'status': upd_record(update_record)}), 200

def upd_record(update_record):

    connection = pymysql.connect(read_default_group='mysql', read_default_file = './mysql.cfg')

    if is_exist_key(update_record['id']):
        try:
            with connection.cursor() as cursor:
                logger.info("更新考勤记录中...")
                # TODO: 字段没协商好
                cursor.execute("update checking_in set num=%s, name=%s, date=%s where id=%s",
                (update_record['student_number'], update_record['name'], update_record['date'], update_record['id']))
                connection.commit()
                logger.info("更新成功")
                connection.close()
                return "Success"
        except Exception as e:
                logger.error("更新考勤记录失败")
                logger.error(e)
                connection.rollback()
                return "Failed"
        finally:
            connection.close()
    else:
        abort(400)


##########################################################################

# 建立人脸库
@app.route('/api/v1/upload-face', methods=['GET', 'POST'])
def upload_face_file():
    if request.method == 'POST':
        # print(request.headers)
        file = request.files['file']
        files = request.files.getlist('file')

        logger.info("正在往人脸库中添加学生头像，信息如下")
        logger.info(files)

        # print(file.filename)
        # print(request.files)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['FACES_DATA_SET'], filename))
            logger.info("人脸头像成功添加入库！")
            return jsonify({"status": "Success"})
        logger.error("人脸头像添加入库失败！")

        return jsonify({"status": "Error"})
    return jsonify({"tips": "please use POST method"})

# 人脸校验服务
# 前端传输过来的只是人脸图像的 base64 编码和图像名
# 大概形式可见 js_client/main.js 文件
@app.route('/api/v1/check-face', methods=['GET', 'POST'])
def jduge_upload_face():
    # print(request.headers)
    if request.method == 'POST':
        file = request.json
        if file and allowed_file(file['filename']):
            filename = secure_filename(file['filename'])
            logger.info("正在进行人脸签到校验...")
            base64_to_img(file['file_base64_str'], file['filename'], app.config['UPLOAD_FOLDER'])
            # TODO: 这里应该做个异常处理
            img_stream = open(app.config['UPLOAD_FOLDER'] + file['filename'], 'rb')
            result, sign_in_name = judge_face_is_in_data_set(img_stream, app.config['FACES_DATA_SET'])
            # print(result)
            if sign_in_name != '':
                create_record(sign_in_name)
                logger.info(file['filename'] + " 签到成功")
            return jsonify(result)
    result = {
        'face_found_in_image': False,
        'sign_in_state': 'Failed'  # 签到状态
    }
    return jsonify(result)

def create_record(name):

    connection = pymysql.connect(read_default_group='mysql', read_default_file = './mysql.cfg')

    try:
        with connection.cursor() as cursor:
            date = strftime('%Y-%m-%d %H:%M:%S', gmtime())  # TODO: 时间不一致
            logger.info("人脸签到：考勤记录添加中...")
            cursor.execute("insert into checking_in(num, name, sex, date, status) values(%s,%s,%s,%s,%s)",\
                    ('1700710135', name, 'male', date, '1'))
            logger.info("人脸签到：考勤记录添加成功")
            connection.commit()
            return "Success"
    except Exception as e:
        logger.error(e)  # 打印错误信息堆栈
        connection.rollback()
        logger.error("人脸签到：添加考勤记录失败")
    finally:
        connection.close()


##########################################################################

@app.errorhandler(400)
def invalid_request(error):
    return make_response(jsonify({'error': 'Bad Request'}), 400)

##########################################################################

# env：init table checking_in
@app.cli.command("create-table",  help='create table checking_in')
def create_table():
    connection = pymysql.connect(read_default_group='mysql', read_default_file = './mysql.cfg')
    try:
        with connection.cursor() as cursor:

            sql = """CREATE TABLE checking_in (
                id INT PRIMARY KEY AUTO_INCREMENT,
                num INT NOT NULL,
                name VARCHAR(10) NOT NULL,
                sex ENUM('male','female') NOT NULL DEFAULT 'male',
                date DATETIME ,
                status ENUM('0','1','2','3') NOT NULL DEFAULT '3')"""

            cursor.execute(sql)
            print("考勤记录表 checking_in 创建成功")
    except Exception as e:
        logging.exception(e)  # 打印错误信息堆栈
        print("考勤记录表 checking_in 创建失败")
    finally:
        connection.close()

# delete table checking_in
@app.cli.command("delete-table",  help='delete table checking_in')
def delete_table():
    connection = pymysql.connect(read_default_group='mysql', read_default_file = './mysql.cfg')
    try:
        with connection.cursor() as cursor:

            sql = "DROP TABLE checking_in"

            cursor.execute(sql)
            print("考勤记录表 checking_in 删除成功")
    except Exception as e:
        logging.exception(e)  # 打印错误信息堆栈
        print("考勤记录表 checking_in 删除失败")
    finally:
        connection.close()

# env：init table api_release
@app.cli.command("create-api",  help='create api info table')
def create_api_info():
    connection = pymysql.connect(read_default_group='mysql', read_default_file = './mysql.cfg')
    try:
        with connection.cursor() as cursor:
            cursor.execute("DROP TABLE apirelease")

            sql1 = """CREATE TABLE apirelease(
                id INT PRIMARY KEY AUTO_INCREMENT,
                version CHAR(5) NOT NULL,
                methods VARCHAR(50) NOT NULL,
                links VARCHAR(150) NOT NULL,
                buildtime DATETIME )"""

            cursor.execute(sql1)

            sql2 = "INSERT INTO apirelease(version, methods, links, buildtime) VALUES(%s,%s,%s,%s)"

            cursor.execute(sql2, ("v1", "GET,POST,PUT,DELETE", "/api/v1/attendance-records", "2020-06-13 01:48:00") )

            print("APIs 信息创建成功")
    except Exception as e:
        logging.exception(e)  # 打印错误信息堆栈
        print("APIs 信息创建失败")
    finally:
        connection.close()

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000, debug=True)